package javacoban;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Bai3 {
    public static void main(String[] args) throws IOException {
            String regex;
            regex = "^\\d*88\\d*89\\d*$";
            ArrayList<PhoneNumber> pn = new ArrayList<PhoneNumber>(10);
            System.out.print("nhap vao ten duong dan den file can doc:");
            Scanner scanner = new Scanner(System.in);
            String nameFileIn = scanner.nextLine();
            File file = new File(nameFileIn);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            FileWriter fw = new FileWriter("out.txt");
            String i;
            int j = 0;
            while((i = br.readLine()) != null){
                String item[] = i.split(",");
                pn.add(new PhoneNumber(item[0], item[1], item[2]));
                j++;
            }
            for(int n = 0; n < j; n++){
                PhoneNumber tmp = pn.get(n);
                if(Pattern.matches(regex, tmp.soDt)){
                    System.out.println(tmp.getSoDt());
                    fw.write(tmp.toString() + "\n");
                }
            }
            fw.close();
            br.close();
            fr.close();
    }
}
