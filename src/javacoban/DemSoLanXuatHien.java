import java.util.Scanner;

public class DemSoLanXuatHien {
    public static void main(String[] args) {
        String chuoi;
        char kyTu = 'a';
        int dem = 0;
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhập vào chuỗi: ");
        chuoi = sc.nextLine();
        for (int i = 0; i < chuoi.length(); i++) {
            if (chuoi.charAt(i) == kyTu) {
                dem++;
            }
        }
        System.out.println("Số lần xuất hiện của ký tự " + "\"" + kyTu + "\"" + " trong chuỗi " + chuoi + " = " + dem);
    }
}
