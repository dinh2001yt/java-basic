package javacoban;

import java.util.Scanner;

public class BtTongHop1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("nhap vao chieu dai canh goc vuong cua tam giac");
        int n = scanner.nextInt();
        int j;
        for(int i = 1; i <= n; i++){
            for(j = 1; j <= i; j++){
                if(j == 1) System.out.print("* ");
                else if(j == i || i == n) System.out.print("* ");
                else System.out.print("  ");
            }
            System.out.println();
        }
    }
}
